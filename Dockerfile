FROM alpine
RUN apk add --update --no-cache python3 graphviz ttf-freefont && ln -sf python3 /usr/bin/python
RUN python3 -m ensurepip && pip3 install --no-cache --upgrade pip setuptools
RUN pip install graphviz
RUN mkdir /app
